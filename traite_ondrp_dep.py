#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division, print_function, absolute_import

import csv
import json

csvfile = csv.DictReader(open('pass/ondrp_dep.csv'), delimiter='\t')

j = {l['dep']:l['code'] for l in csvfile}
out = open('pass/ondrp_dep.json', 'w')
json.dump(j, out, indent=0)
