#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division, print_function, absolute_import

import json
import pandas as pd

def add_xls(out, xlsfile, nmois, pg):
    p = json.load(open("pass/ondrp_dep.json"))
    # evite unicode pour hdf5
    for k,v in p.items():
        p[k] = str(v)
    # optimise pour différents onglets
    xls = pd.ExcelFile(xlsfile)
    for feuille in xls.sheet_names:
        print(feuille)
        # ne parse pas les dom-tom
        df = xls.parse(feuille, parse_cols=98)
        # conversion en date
        df['date'] = pd.to_datetime(df.iloc[:,0], dayfirst=True)
        df['pg'] = pg
        df.rename(columns={df.columns[1]: 'inf'}, inplace=True)
        df =  df.drop([df.columns[0], df.columns[2]], axis=1)
        df.set_index(['date', 'inf', 'pg'], inplace=True)
        df = df.stack()
        df = df[df>0]
        df.index = pd.MultiIndex.from_tuples([(x[0], x[1], x[2], p.get(x[3],x[3])) for x in df.index],
                names=['date', 'inf', 'pg', 'dep'])
        out = out.append(df)
    return out

st = pd.HDFStore('h5/data.h5')
out = pd.Series()
out = add_xls(out, 'data/4-premiers-mois-2014-pn.xlsx',4, 'pn')
out = add_xls(out, 'data/4-premiers-mois-2014-gn.xlsx',4, 'gn')
out = add_xls(out, 'data/2013-fc-pn.xlsx',12, 'pn')
out = add_xls(out, 'data/2013-fc-gn.xlsx',12, 'gn')
out.describe()
out = out.sort_index()
print(out)
st['ondrp2013'] = out
